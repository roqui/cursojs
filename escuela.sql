drop database escuela;
create database escuela;
use escuela;

create table profesores(
    id int primary key auto_increment,
    nombre varchar(200), 
    apellidos varchar(200), 
    antiguedad int
    );

create table materias(
    id int primary key auto_increment,
    materia varchar(200),
    semestre int,
    horas_semana int
);

create table profesores_materias(
    id_profesor int not null,
    id_materia int not null,
    FOREIGN KEY (id_materia) REFERENCES materias(id),
    FOREIGN KEY (id_profesor) REFERENCES profesores(id)
);


