<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Materias</title>
</head>
<body>
    <div class="row">
    <div class="col-4">

    <div class="card" style="margin: 20px 30px 0 30px;">
        <div class="card-header">
            Usuario
        </div>
        <div class="card-body">
            <form action="php/agregar_materia.php" method="POST" id="formMateria">
                <div class="form-group">
                    <label for="inputMateria">Nombre de Materia</label>
                    <input type="text" class="form-control" id="inputMateria" name="materia">
                </div>

                <div class="form-group">
                    <label for="inputSemestre">Semestre</label>
                    <select name="semestre" id="inputSemestre">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="inputHora">Hora</label>
                    <input type="time" class="form-control" id="inputHora" name="horas">
                </div>
                
                <button type="submit" class="btn btn-primary" id="btnEnviar">Enviar</button>
            </form>
        </div>
    </div>

    </div>
    <div class="col-8">
    

    
    </div>
    </div>
</body>
</html>