<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>


    <form action="backend.php" id="formularioPersona">
        <div>
            <label for="nombre"> Nombre</label>
            <input type="text" autocomplete="off" id="nombre" name="nombre">
            <small style="color:red" id="error_nombre"></small>
        </div>

        <div>
            <label for="apellido">Apellido</label>
            <input type="text" autocomplete="off" name="apellido" id="apellido">
            <small style="color:red" id="error_apellido"></small>
        </div>

        <div>
            <label for="telefono">Telefono</label>
            <input type="text" autocomplete="off" name="telefono" id="telefono">
        </div>

        <div>
            <label for="email">Email</label>
            <input type="email" autocomplete="off" name="email" id="email">
            <small style="color:red" id="error_email"></small>
        </div>

        <div>
            <label for="profesor">Profesor</label>
            <input type="checkbox" autocomplete="off" name="profesor" id="profesor">
            
        </div>

        <div id="div_profesor" hidden>
            <div>
                <label for="materia">Materia</label>
                <input type="text" id="materia" name="materia">
                <small style="color:red" id="error_materia"></small>
            </div>

            <div>
                <label for="semestre">Semestre</label>
                <input type="text" id="semestre" name="semestre">
            </div>
        </div>

        
        <div>
            <button type="submit" id="btn-enviar">
                enviar
            </button>
        </div>
    </form>

    <!-- <script src="app.js"></script> -->
    <script>
        let btn_enviar = document.getElementById('btn-enviar');
        btn_enviar.onclick = enviar;

        const formulario = document.getElementById('formularioPersona');
        let error_nombre = document.getElementById('error_nombre');
        let error_apellido = document.getElementById('error_apellido');
        let error_email = document.getElementById('error_email');
        let error_materia = document.getElementById('error_materia');

        formulario.profesor.onchange = profesor;

        function enviar(evento){
            event.preventDefault();
            console.log('funcion enviar')

            if(formulario.nombre.value == "" 
            || formulario.apellido.value == "" 
            || formulario.email.value == ""
            || (formulario.profesor.checked == true && formulario.materia.value == "")){
                
                formulario.nombre.value == "" ? error_nombre.innerText = "Campo obligatorio" : error_nombre.innerText = ""; 
                formulario.apellido.value == "" ? error_apellido.innerText = "Campo obligatorio" : error_apellido.innerText = ""; 
                formulario.email.value == "" ? error_email.innerText = "Campo obligatorio" : error_email.innerText = ""; 

                if(formulario.profesor.checked == true){
                    formulario.materia.value == "" ? error_materia.innerText = "Campo obligatorio" : error_materia.innerText = "";
                }else{
                    error_materia.innerText = "";
                }

            }else{
                limpiar()
            }

            console.log(formulario.profesor.checked);

        }

        function limpiar(){
            
            formulario.nombre.value = "";
            formulario.apellido.value = "";
            formulario.email.value= "";
            formulario.telefono.value ="";
            formulario.materia.value = "";
            formulario.semestre.value = "";

            error_apellido.innerText = "";
            error_email.innerText = "";
            error_nombre.innerText = "";
            error_materia.innerText = "";
        }

        function profesor(){
            console.log("Funcion Profesor");
            div_profesor = document.getElementById('div_profesor');
            console.log(div_profesor);
            div_profesor.hasAttribute('hidden') ? div_profesor.removeAttribute('hidden') : div_profesor.setAttribute('hidden',"");
        }
        

    </script>
</body>
</html>