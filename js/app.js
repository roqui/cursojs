{

    const numeros = document.querySelectorAll('.numeros');
    numeros.forEach(numero => numero.onkeypress = soloNumeros);

    //Solo permite introducir números.
    function soloNumeros(e){
        let key = e.charCode;
        console.log(e);
        return key >= 48 && key <= 57;
    }


    // Scope
    // El SCOPE siempre esta definido por llaves, clases o modulos
    // llaves de funciones o clases
    // modulos: "librerías / extensiones / archivos" reutilizables
    
    // TIPOS VARIABLES

    // VAR: utilizada cuando surgio JS
    // No respeta ningun scope (funciones, clases, modulos)
    var saludo = "Hola!";
    // Re-declarable y Re-asginable
    var saludo = "Alo!";
    saludo = "Que tal!";

    // LET: Funciona como var pero respeta los SCOPES
    let despedida = "Adios";
    // No es re-definible pero es re-asignable
    despedida = "Hasta luego!";

    // CONST: Solo se puede declarar y asignar en la misma linea, una sola vez!
    const comentario = "Te ves bien!";
}

