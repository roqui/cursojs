<?php 

    include_once('php/conexion.php');
    echo $id_profesor = $_GET['id_profesor'];
    


    $query = "select * from profesores where id = $id_profesor";
    $result = mysqli_query($conexion,$query);
    if(mysqli_num_rows($result) == 1){
        $profe = mysqli_fetch_array($result);
        // echo $profe['nombre'];
        echo $profe[1];
        
        // {
        //     id: 5,
        //     nombre: 'rodrigo',
        //     apellidos: 'Quintana',
        //     antiguedad: 12
        // }

    }

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">


    <title>Curso JS!</title>
</head>

<body class="container">

    <?php include_once('layouts/menu.php'); ?>


    <div class="card" style="margin: 20px;">
        <h5 class="card-header">Profesores</h5>
        <div class="card-body">

            <form action="php/editar_profesor_metodo.php" method="POST">
                <div class="form-group">
                    <label for="inputId">Id</label>
                    <input type="text" class="form-control" id="inputId" value="<?php echo $profe['id'] ?>" name="id" readonly>
                </div>

                <div class="form-group">
                    <label for="inputNombre">Nombre</label>
                    <input type="text" class="form-control" id="inputNombre" value="<?php echo $profe['nombre'] ?>" name="nombre">
                </div>

                <div class="form-group">
                    <label for="inputApellidos">Apellidos</label>
                    <input type="text" class="form-control" id="inputApellidos" value="<?php echo $profe['apellidos'] ?>" name="apellidos">
                </div>

                <div class="form-group">
                    <label for="inputAntiguedad">Antiguedad</label>
                    <input type="text" maxlength="2" max class="form-control numeros" value="<?php echo $profe['antiguedad'] ?>" id="inputAntiguedad" name="antiguedad">
                </div>

                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>

        </div>
    </div>

    <?php include_once('layouts/footer.php'); ?>

    <!-- scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
</body>

</html>