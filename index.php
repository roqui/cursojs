<?php include_once('php/consulta-profesores.php'); ?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">


    <title>Curso JS!</title>
</head>

<body class="container">

    <?php include_once('layouts/menu.php'); ?>


    <div class="card" style="margin: 20px;">
        <h5 class="card-header">Profesores</h5>
        <div class="card-body">

            <form action="php/agregar.php" method="POST">

                <div class="form-group">
                    <label for="inputNombre">Nombre</label>
                    <input type="text" class="form-control" id="inputNombre" name="nombre">
                </div>

                <div class="form-group">
                    <label for="inputApellidos">Apellidos</label>
                    <input type="text" class="form-control" id="inputApellidos" name="apellidos">
                </div>

                <div class="form-group">
                    <label for="inputAntiguedad">Antiguedad</label>
                    <input type="text" maxlength="2" max class="form-control numeros" id="inputAntiguedad" name="antiguedad">
                </div>

                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>

        </div>
    </div>

    <div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre(s)</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Antiguedad</th>
                    <th scope="col"></th>

                </tr>
            </thead>
            <tbody>

                <?php foreach ($result as $profesor) { ?>
                    <tr>
                        <th scope="row"><?php echo $profesor['id'] ?></th>
                        <td><?php echo $profesor['nombre'] ?></td>
                        <td><?php echo $profesor['apellidos'] ?></td>
                        <td><?php echo $profesor['antiguedad'] ?></td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal<?php echo $profesor['id'] ?>">
                                Eliminar
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal<?php echo $profesor['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">¿Eliminar <?php echo $profesor['nombre'] ?>?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-footer">
                                            <form action="php/eliminar.php" method="POST">
                                                <input type="text" value="<?php echo $profesor['id'] ?>" name="id_profesor" hidden>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <form action="editar_profesor_vista.php">
                                <input type="text" value="<?php echo $profesor['id'] ?>" name="id_profesor" hidden>
                                <button class="btn btn-info">
                                    Editar
                                </button>
                            </form>
                            <a href="editar_profesor_vista.php?id_profesor=<?php echo $profesor['id'] ?>">
                            <button class="btn btn-secondary">
                                Editar 2
                            </button>
                            </a>

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>




    <?php include_once('layouts/footer.php'); ?>

    <!-- scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
</body>

</html>